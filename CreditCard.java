package com.transaction.SpringTransactionDemo.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import org.springframework.http.codec.CodecConfigurer.CustomCodecs;

@Entity
public class CreditCard {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "creditcard_ID")
	private Long id;
	
	
    private String cardNumber;
	
    private String expiryMonth;
	
    private String expiryYear;
	
    private String securityCode;

	@OneToOne(fetch = FetchType.EAGER,cascade=CascadeType.ALL)
	
	private CustomCodecs cardHolder;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}



	public String getExpiryMonth() {
		return expiryMonth;
	}

	public void setExpiryMonth(String expiryMonth) {
		this.expiryMonth = expiryMonth;
	}

	public String getExpiryYear() {
		return expiryYear;
	}

	public void setExpiryYear(String expiryYear) {
		this.expiryYear = expiryYear;
	}

	public String getSecurityCode() {
		return securityCode;
	}

	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}

	public CustomCodecs getCardHolder() {
		return cardHolder;
	}

	public void setCardHolder(CustomCodecs cardHolder) {
		this.cardHolder = cardHolder;
	}
	
	
}